﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OteroJoshua_CE01
{
    public class DevClassEventArgs : EventArgs
    {
        public string ClassName;

        public bool Completed;
    }
}
