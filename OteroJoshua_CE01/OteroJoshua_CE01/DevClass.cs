﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OteroJoshua_CE01
{
    class DevClass
    {
        public string ClassName;

        public bool Completed;

        public override string ToString()
        {
            return $"{ClassName}";
        }
    }
}
